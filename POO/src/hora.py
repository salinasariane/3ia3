#!/usr/bin/env python3
class Hora:
    horas=0
    minutos=0
    segundos=0

    def imprime_hora(self):
        print("La hora es",str(self.horas),":",str(self.minutos),":",str(self.segundos))


    def incrementa_hora(self, seg):
        self.segundos+=seg
        while self.segundos>=60:
            self.segundos-=60
            self.minutos+=1
        while self.minutos>=60:
            self.minutos-=60
            self.horas+=1
        self.imprime_hora()

    def comparacion(self, hora2):
        if self.horas>hora2.horas:
            return 1
        elif self.horas==hora2.horas:
            if self.minutos>hora2.minutos:
                return 1
            elif self.minutos==hora2.minutos:
                if self.segundos==hora2.segundos:
                    return 0
                elif self.segundos>hora2.segundos:
                    return 1
                else:
                    return -1
            else:
                return -1
        else:
            return -1
            



h=Hora()
h.horas=3
h.minutos=24
h.segundos=45

h2=Hora()
h2.horas=3
h2.minutos=25
h2.segundos=45

print(h.comparacion(h2))