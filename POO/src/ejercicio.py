#!/usr/bin/env python3

class Punto:
    x, y=0,0

class Rectangulo: 
    ancho, alto =0,0
    esquina=Punto()

def mueveRect(rectangulo, dx, dy):
    rectangulo.esquina.x=dx
    rectangulo.esquina.y=dy
    return rectangulo


r= Rectangulo()
r.ancho=10
r.alto=15
r.esquina.x=2
r.esquina.y=7
mueveRect(r,6,8)
print(r.esquina.x)