#!/usr/bin/env python3


#EJERCIICIO 1----------------

class Persona:
    def __init__(self, nombre="", edad=0, DNI=''):
        self.__nombre=nombre
        self.__edad=edad
        self.__DNI=DNI
    @property
    def nombre(self):
        return self.__nombre
    
    @nombre.setter
    def nombre(self, nuevo):
        self.__nombre=nuevo
 
    @property
    def edad(self):
        return self.__edad

    @edad.setter
    def edad(self, nuevo):
        self.__edad=nuevo

    @property
    def DNI(self):
        return self.__DNI

    @DNI.setter
    def DNI(self, nuevo):
        self.__DNI=nuevo
    
    def mostrar(self):
        print(str(self.__nombre),int(self.__edad), str(self.__DNI))
    
    def esMayorDeEdad(self):
        if self.__edad>=18:
            return True
        else:
            return False
   


p1=Persona("Ariane", 19, '19864786Y')
p1.mostrar


#EJERCICIO 2---------------
class Cuenta:
    titular=Persona()
    def __init__(self, titular=p1, cantidad=0):
        self.__titular=titular
        self.__cantidad=cantidad

    @property
    def titular(self):
        return self.__titular
    @titular.setter
    def titular(self, nuevo):
        self.__titular=nuevo
    @property
    def cantidad (self):
        return self.__cantidad
    
    @cantidad.setter
    def cantidad(self, nuevo):
        self.__cantidad=nuevo

    def mostrar(self):
        return self.__cantidad

    def ingresar(self, cantidad):
        if cantidad>=0:
            self.cantidad+=cantidad
    
    def retirar (self, cantidad):
        if self.cantidad>=0:
            self.cantidad-=cantidad

c=Cuenta()
print(c.titular.nombre)

#EJERCICIO 3---------------------------
class cuenta_joven(Cuenta):
    def __init__(self,titular, cantidad, bonificacion):
        self.__bonificacion=bonificacion
        super().__init__(titular, cantidad)
    @property
    def bonificacion (self):
        return self.__bonificacion
    
    @bonificacion.setter
    def bonificacion(self, nuevo):
        self.__bonificacion=nuevo

    def esTitularValido(self):
        if self.titular.edad>18 and self.titular.edad<25:
            print("Es un titular valido")
            return True
        else: 
            return False
    def retirar(self, cantidad):
        if self.cantidad>=0 and self.esTitularValido:
            self.cantidad-=cantidad
        else:
            print("No puede retirar")

#EJERCICIO 4----------------
    
class Vehiculo:
    def __init__(self, color="", ruedas=0):
        self.color=color
        self.ruedas=ruedas

    def catalogar(vehiculos):
        for v in vehiculos:
            print("")

    def catalogar1(vehiculos, ruedas=0):
        vehiculos_ruedas=[]
        for v in vehiculos:
            if v.ruedas==ruedas:
                vehiculos_ruedas.append(v)
                print("")
        print("Cantidad de coches con ",ruedas,"ruedas= ",len(vehiculos.ruedas))


class Coche(Vehiculo):
    def __init__(self, velocidad=0, cilindrada=0, color='', ruedas=0):
        self.velocidad=velocidad
        self.cilindrada=cilindrada
        super().__init__(color, ruedas)

class Camionera(Coche):
    def __init__(self, carga=0, color='', ruedas=0, velocidad=0, cilindrada=0):
        self.carga=carga
        super().__init__(color, ruedas, velocidad, cilindrada)

class Bicicleta(Vehiculo):
    def __init__(self, tipo="", color='', ruedas=0):
        self.tipo=tipo
        super().__init__(color, ruedas)

class Motocicleta(Bicicleta):
    def __init__(self, velocidad=0, cilindrada=0, color='', ruedas=0, tipo=''):
        self.velocidad=velocidad
        self.cilindrada=cilindrada
        super().__init__(color, ruedas, tipo)

co=Coche(50, 105, "Rojo", 4)
ca=Camionera(355, "blanco", 4,80,355)
b=Bicicleta("Urbano", "negro", 2)
m=Motocicleta(40,20, "Blanco", 2, "Deportiva")
vehiculos=[co, ca, b, m]






#EJERCICIO 5----------------- diria ke mal asik comprobar
class Adder:
    def add(self,x,y):
        print("Not implemented")
class ListAdder(Adder):
    def add(self,x='',y=''):
        return x+y
class DictAdder(Adder):
    def add(self,x='',y=''):
        return {x:y}

#EJERCICIO 6----------------------

class Motocicleta:
    def __init__(self, color="", matricula="", combustible_litros=0, numero_ruedas=2, marca="", modelo="", fecha_fabricacion=0, velocidad_punta=0, peso=0, motor=False):
        self.color=color
        self.matricula=matricula
        self.combustible_litros=combustible_litros
        self.numero_ruedas=numero_ruedas
        self.marca=marca
        self.modelo=modelo
        self.fecha_fabricacion=fecha_fabricacion
        self.peso=peso
        self.velocidad_punta=velocidad_punta
        self.motor=motor


    def arrancar(self):
        if self.motor:
            print("El motor ya estaba arrancado")
        else:
            print("Motor arrancado")
            self.motor=True

    def detener(self):
        if self.motor:
            print("El motor detenido")
            self.motor=False
        else:
            print("Motor ya estaba detenido")

m=Motocicleta(combustible_litros = 10, numero_ruedas=2)