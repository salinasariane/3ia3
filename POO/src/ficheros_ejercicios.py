#!/usr/bin/env python3
f=open('ejemplo.txt')
#EJERCICIO 1--------
def leer_fichero(nombre):
    f=open(nombre, "r")
    return f.read()

#EJERCICIO 2--------
def file_read_from_head(fich, num):
    num-=1
    while num>=0:
        print(fich.readline())
        num-=1
#file_read_from_head(f,2)

#EJERCICIO 3 ------
def añadir_linea(fich, linea1, linea2):
    fichero=open(fich, 'a')
    fichero.write(linea1)
    fichero.write(linea2)
    fichero.close()
#añadir_linea("ejemplo.txt","jaja", "jeje")


#EJERCICIO 4---------
def file_read_from_tail(fich, num):
    with open(fich, 'r') as fichero:
        lista_lineas=fichero.readlines()
        for i in range(len(lista_lineas)-1, len(lista_lineas)-num-1, -1):
            print(lista_lineas[i])


#EJERCICIO 5---------
def file_read(fich):
    linea=fich.readline()
    lista=[]
    while linea:
        lista.append(linea)
        linea=fich.readline()
    print(lista)
#file_read(f)

#EJERICICO 6----------
def file_read2(fich):
    lineas= fich.readlines()
    lista=""
    for l in lineas:
        lista+=str(l)
    print(lista)
#file_read2(f)

#EJERCICIO 7--------
def longest_word(fich):
    linea=fich.read()
    lista=""
    while linea:
        lista+=str(linea)+" "
        linea=fich.readline()
    x= lista.split(" ")
    longitud=0
    palabra=[]
    for y in x:
        if len(y)>longitud:
            longitud=len(y)
    for z in x:
        if longitud==len(z):
            palabra.append(z)
    print(palabra)
longest_word(f)

#EJERCICIO 8------SI
def file_length(fich):
    contador=0
    while fich.readline():
        contador+=1
    return contador
#print(file_length(f))


#EJERCICIO 9----------SI
def palabras(fich, palabra):
    fichero=fich.readlines()
    contador=0
    string=""
    for f in fichero:
        string+=f.rstrip()
        if palabra in string:
            contador+=1
    return contador
#print(palabras(f, "jajajeje"))


#EJERCICIO 10 ----------- SI
def lista(lista, fich):
    fichero=open(fich, "w")
    for l in lista:
        fichero.write(str(l)+ "\n")
    fichero.close()
l=["r", "j"]
lista(l,"ejemplo2.txt")


#EJECICIO 11 ----------SI
def copiar(fich1, fich2):
    lineas= fich1.readlines()
    lista=""
    fichero2=open(fich2,"w")
    for l in lineas:
        fichero2.write(l)
    fichero2.close()
    print(lista)
#copiar(f,"ejemplo2.txt")

#EJERCICIO 12--------------RARO
import random
def random_line(fich):
    filename=open(fich, "r")
    random.seed()
    print(filename.readline(random.randint(0,len(filename.readline()))))
#random_line("ejemplo.txt")


#EJERCICIO 13--------- SI
def cerrado(fich):
    if fich.closed:
        return True
    else:
        return False
#print(cerrado(f))