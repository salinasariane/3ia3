#!/usr/bin/env python3


class Carta:
    listaDePalos = ["Tréboles", "Diamantes", "Corazones","Picas"]
    listaDeValores = ["nada", "As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Sota", "Reina", "Rey"]
    def __init__(self, palo=0, valor=0):
        self.palo = palo
        self.valor = valor
    def __str__(self):
        return (self.listaDeValores[self.valor] + " de " +
                    self.listaDePalos[self.palo] )
"""
    def __cmp__(self, n):
        if n.valor==1:
            return -1
        if self.valor==1:
            return 1
"""
"""
    def __cmp__ (self,n):
        if self.valor>n.valor:
            return 1
        if self.valor<n.valor:
            return -1
        else:
            return 0

"""
c=Carta(0,1)
c1=Carta(3,12)

#print(c.__cmp__(c1))


class Mazo:
    cartas=[]
    def __init__(self):
        for palo in range (4):
            for valor in range (1,14):
                self.cartas.append(Carta(palo, valor))
    def __str__(self):
        cadena=''
        for carta in self.cartas:
            cadena+=str(carta)+'\n'
        return cadena

    def mezclar(self):
        import random
        nCartas = len(self.cartas)
        for i in range(nCartas):
            j = random.randrange(i, nCartas)
            self.cartas[i], self.cartas[j] = self.cartas[j], self.cartas[i]

    def eliminaCarta(self, carta):
        if carta in self.cartas:
            self.cartas.remove(carta)
            return 1
        else:
            return 0

    def darCarta(self):
        self.mezclar()
        carta1= self.cartas.pop()
        return carta1

    def estaVacio(self):
        if len(self.cartas)==0:
            print("esta vacio")
            return True
        else:
            print("No esta vacio")
            return False

    def repartir(self):
        self.mezclar()
        for carta in self.cartas:
            print(self.darCarta())

mazo =Mazo()
mazo.repartir()