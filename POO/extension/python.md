


# Python extension 

Python es una extensión de VSCode que ayuda a programar con Python de distintas maneras como autocompletando, buscando posibles errores o depurando

Link: https://github.com/Microsoft/vscode-python

### Instalación
Para instalarlo desde VSCode al entrar a la aplicacion, hay que darle a la izquierda la quinta opcion de la columna o pulsando Ctrl+Shift+X. Ahi arriba buscar Python e instalarlo (comprobar que es el correcto)


### Caracteristicas y uso 

1. Autocompletado
2. Linting -> Analiza el codigo buscando posibles errores
3. Debugging -> establece puntos de interrupcion
4. Cuadernos Jupyter -> Al abrir un archivo .ipynb se puede usar  Jupyter Notebook Editor
5. Testing -> Esta extension admite pruebas con unittest y pytest (hay que importarlos )

```python
import unittest


class SimplisticTest(unittest.TestCase):

    def test(self):
        a = 'a'
        b = 'a'
        self.assertEqual(a, b)
```

Comando para ejecutarlo: python3 -m unittest unittest_simple.py

### Conclusiones 
Es una buena extensión sobre todo para empezar a programar con Python ya que ayuda bastante tanto a la hora de estar programando como a solucionar los problemas que van saliendo
