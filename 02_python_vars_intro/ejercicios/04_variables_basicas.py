#!/usr/bin/env python3

#EJERCICIO 1
carname= "Volvo"

#EJERCICIO 2
x=50

#EJERCICIO 3
x=5
y=10
print(x+y)

#EJERCICIO 4
z=x+y
print(z)

#EJERCICIO 5
#2my-first_name = “John” #ERROR
my_first_name = "John"

#EJERCICIO 6
#x _ y _ z _"Orange" #ERROR
x=z=y="Orange"

#EJERCICIO 7
cars=100
space_in_a_car=5
drivers=30
passengers=50

cars_not_driven=cars-drivers
carpool_capacity=drivers*space_in_a_car
media=carpool_capacity/passengers

print(f"Hay {cars} coches disponibles")
print(f"Hay {drivers} conductores disponibles")
print(f"Hay {cars_not_driven} coches vacios")
print(f"Hay {passengers} pasajeros")
print(f"Podemos trasportar {carpool_capacity} pasajeros")
print(f"Van {media} pasajeros por coche")

#EJERCICIO 8
cadena="james"
cadena_nueva=cadena[0]+cadena[int((len(cadena)-1)/2)]+cadena[int(len(cadena)-1)]
cadena_nueva=cadena[0]+cadena[(len(cadena)-1)//2]+cadena[-1] #si pones // te devuelve int y el -1 te devuelve a la inversa aka el ultimo
print(cadena_nueva)

#EJERCICIO 9
cadena="Paula"
posicion_caracter_medio=int((len(cadena)-1)/2)
cadena_corta=cadena[posicion_caracter_medio-1]+cadena[posicion_caracter_medio]+cadena[posicion_caracter_medio+1]
print(cadena_corta)

#EJERCICIO 10
cadena1="America"
cadena2="Japon"
cadenafinal=cadena1[0]+cadena2[0]+cadena1[int((len(cadena1)-1)/2)]+cadena2[int((len(cadena2)-1)/2)]+cadena1[len(cadena1)-1]+cadena2[len(cadena2)-1]
print(cadenafinal)