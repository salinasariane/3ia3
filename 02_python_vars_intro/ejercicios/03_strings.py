#!/usr/bin/env python3

#EJERCICIO 1
nombre= "Ariane"
print(f"hola {nombre}, ¿Te gustaria aprender algo de Python hoy?")
print(f"hola "+ nombre +"¿Te gustaria aprender algo de Python hoy?")
print(f"hola ",nombre," ¿Te gustaria aprender algo de Python hoy?")

#EJERCICIO 2
nombre="ariane"
print(nombre.capitalize())
print(nombre.upper())
print(nombre.lower())

#EJERCICIO 3
print('Albert Einstein dijo una vez: \"Una persona que nunca hizo una error nunca probé nada nuevo.\"')


#EJERCICIO 4
nombre_autor="Albert Einstein"
cita="Una persona que nunca hizo una error nunca probé nada nuevo."
print(f'{nombre_autor} dijo una vez: "{cita}"')

#EJERCICIO 5
spc= "  \n\t  espacio   \n  "
print(spc.strip())


