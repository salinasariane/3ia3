#!/usr/bin/env python3
p = {'nombre': "Paula",
    'apellido': "Lopez",
    'edad': 20,
    'ciudad':"Tenerife"
    }
print(p)
print(p["nombre"])
print(p["apellido"])
print(p["edad"])
print(p["ciudad"])


n = {'Laura': 15,
    'Iker': 2,
    'Jimena': 20,
    'Mikel':7,
    'Ainhoa':23
    }
print(n)
print(n["Laura"])
print(n["Iker"])
print(n["Jimena"])
print(n["Mikel"])
print(n["Ainhoa"])
