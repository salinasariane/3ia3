#!/usr/bin/env python3

#EJERCICIO 1
nombres = ["Paula","laura", "Marina","Patricia", "Oihana"]
print(nombres[0])
print(nombres[1])
print(nombres[2])
print(nombres[3])
print(nombres[4])

#EJERCICIO 2
nombres = ["Paula","laura", "Marina","Patricia", "Oihana"]
print("Hola "+nombres[0].title()+", quetal?")
print("Hola "+nombres[2].title()+", quetal?")

#EJERCICIO 3
marcas=["Honda", "Renault", "Ford"]
print("Me gustaria tener una moto "+marcas[1].title())

#EJERCICIO 7
invitados = ["Paula","Jon", "Oihana"]
print(invitados[0].title()+ " esta invitado a la fiesta")
print(invitados[1].title()+ " esta invitado a la fiesta")
print(invitados[2].title()+ " esta invitado a la fiesta")


print(invitados[0].title()+ " no puede asistir")
invitados[0]="Juan"
print(invitados[0].title()+ " vendra en su lugar")
invitados.insert(0, "Mikel")
invitados.insert(2, "Markel")
invitados.append("Olatz")


print(invitados[0].title()+ " esta invitado a la fiesta")
print(invitados[2].title()+ " esta invitado a la fiesta")
print(invitados[5].title()+ " esta invitado a la fiesta")


print("Solo pueden venir dos personas")
invitados.pop(0)
invitados.pop(2)
invitados.pop(3)
invitados.pop(1)
print(invitados[0].title()+ " vendra a la fiesta")
print(invitados[1].title()+ " vendra a la fiesta")

del(invitados[0])
del(invitados[0])
print(*invitados)

