#!/usr/bin/env python3

lista=['zero', 'nombre', 'nombre']
lista.remove('nombre')
print(lista)
lista.sort()
print(lista.reverse())

lista1=[1,2,3,8,5,6]
lista1.sort()
print(lista1.reverse())

sitios=['Berlin', 'Praga', 'Paris','Barcelona','Roma']

print(sitios)

print(sorted(sitios))

print(sitios)

print(sorted(sitios).reverse())

print(sitios)

sitios.reverse()
print(sitios)

sitios.reverse()
print(sitios)

sitios.sort()
print(sitios)

sitios.reverse()
print(sitios)


pizzas=["peperoni","de la huerta","barbacoa"]
for pizza in pizzas:
    print (pizza)

for pizza in pizzas:
    print ("Me gusta la pizza "+pizza)



for pizza in pizzas:
    print ("Me gusta la pizza "+pizza)
print("\nMe gusta mucho")



animales=["perro","gato","oveja"]
for animal in animales:
    print (animal)

for animal in animales:
    print ("Mi mascota preferida es el "+animal)



for animal in animales:
    print ("Mi mascota preferida es el "+animal)
print("\n“¡Cualquiera de estos animales sería una gran mascota!.")


x = range(1, 21)
for n in x:
  print(n)

y = range(1, 1000001)
for n in y:
  print(n)

print(min(y)) 
print(max(y)) 
print(sum(y))

print("impares")
digitos = range(1, 21,2)
for d in digitos:
    print (d)

print("multiplos del 3")
numeros = range(3, 31,3)
for n in numeros:
    print (n)

print("cubos")
nums=range(0,11)
for m in nums:
    print(m**3)