#!/usr/bin/env python3


#EJERCICIO 1
num_str=input("Introduce un numero: ")
num=int(num_str)
if num%2==0:
    print("El numero es par")
else:
    print("Es impar")


print("\n-----------------------------------------")

#EJERCICIO 2

x=16
y=14
if x+y==30:
    print("La suma de ambos es 30")
elif x==30 or y==30:
    print("uno de los numeros es 30")
else: 
    print("No se cumple ninguna condicion")


print("\n-----------------------------------------")
#EJERCICIO 3
dia=str(input("introduce el dia de la semana")).lower()
if dia=="lunes":
    print("1")
elif dia=="martes":
    print("2")
elif dia=="miercoles":
    print("3")
elif dia=="jueves":
    print("4")
elif dia=="viernes":
    print("5")
elif dia=="sabado":
    print("6")
elif dia=="domingo":
    print("7")
else:
    print("Eso no es un dia de la semana")


print("\n-----------------------------------------")
#EJERCICIO 4
unidades=175
if unidades<50:
    precio=unidades*3.5
if unidades>50 and unidades<150:
    un=unidades-50
    precio=50*3.5+4*un
if unidades>150 and unidades<250:
    un=unidades-150
    precio=50*3.5+4*100+5.2*un
else:
    un=unidades-250
    precio=50*3.5+4*100+5.2*100+6.4*un
print(precio)

print("\n-----------------------------------------")
#EJERCICIO 5
numE_str=input("Introduce un numero: ")
numE=int(numE_str)
if numE>0:
    print("El numero es mayor que 0")
elif numE==0:
    print("El numero es 0")
else:
    print("El numero es menor que0")


print("\n-----------------------------------------")

#EJERCICIO 6
x="abd"
y="pabde"

if x in y:
    print("La cadena si esta dentro")
else:
    print("la cadena no esta dentro")


print("\n-----------------------------------------")
#EJERCICIO 7
cesta = {	
    'boli':
	{
		'marca' : "Bic",
		'precio'  : "0.75€",
		'referencia'  : "552BIC12"
    },
    'lapiz':
	{
		'marca' : "Pritt",
		'precio' : "1.75€",
		'referencia'  : "567PRI13"
	}
}

for x in cesta:
    y=cesta[x]
    print (x, y)

for x in cesta:
    print(cesta[x])
    for key, val in cesta[x].items():
        print (key,val)

print("\n-----------------------------------------")
#EJERCICIO 7.1
cesta["cuaderno"]={"libreta":"roja"}
print(cesta)

print("\n-----------------------------------------")
#EJERCICIO 8
numero1=int(input("Introduce un numero (8) "))
suma=0

while numero1>0:
    suma+=numero1
    numero1=numero1-1
print(suma)

for x in range(1,numero1+1):
    suma+=x
print(suma)
print("\n-----------------------------------------")

#EJERCICIO 9
s="Ariane"
lista_nums=[]

for n in range (0,len(s)):
    if n%2==0:
       print(s[n])


print("\n-----------------------------------------")
#EJERCICO 10
cadena1= "Pynative"
new_string = cadena1.lstrip("Pyna")
print(new_string)
print("\n-----------------------------------------")
#EJERCICIO 11
lista=[1,2,3,4,5,1]
if lista[0]==lista[-1]:
    print("Son iguales")
else:
    print("No lo son")


print("\n-----------------------------------------")
#EJERCICIO 12
lis=[12,34,55,65,23,70]
for a in lis:
    if a%5==0:
        print(a,"es divisible entre 5")


print("\n-----------------------------------------")
#EJERCICIO 13
num13=int(input("Introduce un numero (13): "))
for i in range(1,num13+1):
    for j in range(1, i+1):
        print(i, end=" ")
    print()
    



print("\n-----------------------------------------")

#EJERCICIO 14
num14=input("Introduce un numero(14): ")
print(int(str(num14)[::-1]))

#EJERCICIO 15
num15=int(input("Introduce un numero (13): "))
for a in reversed(range(1,num15+1)):
    for b in range(1, a+1):
        print("*", end="")
    print()
    

print("\n-----------------------------------------")
#EJERCICIO 16
num16=input("Introduce una serie de numero separados por coma: ")
print(num16.split(","))

print("\n-----------------------------------------")
#EJERCICIO 17
name="py.exe"
print(name.split(".")[-1])
print("\n-----------------------------------------")