#!/usr/bin/env python3

#EJERCICIO 1
x=900
y=50
z=40
def maximo (a,b,c):
    if a>b and a>c:
        print("El numero maximo es ", a)
    elif b>c:
        print("El numero maximo es ", b)
    else:
        print("El numero maximo es ", c)
#maximo (x,y,z)

#EJERCICIO 2
def sumar(nums):
    suma=0
    for n in nums:
        suma+=n
    print ("La suma de los numero es ",suma)
numes=[1,3,5,7,14]
#sumar(numes)

#EJERCICIO 3 
def multiplicar(nums):
    mult=1
    for s in nums:
        mult=mult*s
    print ("La multiplicacion de los numero es",mult)
numes=[1,3,5,7]
multiplicar(numes)


#EJERCICIO 4
cadena="ABCdef23"
def revertir (cad):
    cad_rev=''.join(reversed(cad))
    print(cad_rev)

def revertir2(cad):
    return (cadena[::-1])

#revertir(cadena)

#EJERCICIO 5
def factorial (num):
    valor=0
    num1=num
    while num1>=1:
        valor+=num
        num1-=1
    print("El factorial de",num," es", valor)
#factorial(5)

def recur_factorial():
    print('recur_factorial(,',n,')')
    if n==1:
        return n
    else:
        return n*recur_factorial(n-1)
    

#EJERCICIO 6-----------MAL
def rango(num1, num2, num3):
    if num1>num2 and num1<num3:
        print(num1," esta en el rango entre", num2, "y ", num3)
rango(7,2,6)

#EJERCICIO 7
def conta(cadena3):
    minus=0
    mayus=0
    for c in cadena3:
        if c==c.lower():
            minus+=1
        else:
            mayus+=1
    print("Hay ",minus,"minusculas y ", mayus,"mayusculas")
conta("JuJuJJuuJhJ")


#EJERCICIO 8
def repetidos (numeros):
    for c in numeros:
        if c in numeros:
            numeros.remove(c)
    print(numeros)
repetidos([1,2,3,3,3,4,5,5])

#EJERCICIO 9
def es_primo(num):
    for n in range(2, num):
        if num % n == 0:
            print("No es primo", n, "es divisor")
    print("Es primo")
#es_primo(5)

#EJERCICIO 10
def pares(numeros1):
    lista=[]
    for num in numeros1:
        if num%2==0:
            lista.append(num)
    print(lista)

pares([1,2,3,4,5,6,7,8,9])

#EJERCICIO 11
def perfecto(n):
    n1=n
    divisores=[]
    suma=0
    while n1>1:
        n1=n1-1
        if n % n1 == 0:
            divisores.append(n1)
        
    for d in divisores:
        suma+=d
    if suma==n:
        print("El numero es perfecto")

perfecto(10)

#EJERCICIO 12
def palindromo(str):
    str=str.lower()
    str_rev=''.join(reversed(str))
    if str==str_rev:
        print(str, "es palindromo")

palindromo("anna")


#EJERCICIO 13 -----------???????????
def pascal_tri(numRows):
    if numRows == 1:
        return [[1]] # base case is reached!
    else:
        res_arr = pascal_tri(numRows-1) # recursive call to pascal_tri
        # use previous row to calculate current row 
        cur_row = [1] # every row starts with 1
        prev_row = res_arr[-1] 
        for i in range(len(prev_row)-1):
            # sum of 2 entries directly above
            cur_row.append(prev_row[i] + prev_row[i+1]) 
        cur_row += [1] # every row ends with 1
        res_arr.append(cur_row)
        return res_arr
tri_array = pascal_tri(5)

for i,row in enumerate(tri_array):
  for j in range(len(tri_array) - i + 1):
    print(end=" ") # leading spaces
  for j in row:
    print(j, end=" ") # print entries
  print("\n")  # print new line


#EJERCICIO 14
def es_pangrama(cadena):
    import string
    cadena = cadena.lower() 
    alfabeto = string.ascii_lowercase + "ñ" 
    for letra in alfabeto: 
        if letra not in cadena:
            return False
        else:
            print(cadena, "es panagrama")
            return True

es_pangrama("bcd pdj")
es_pangrama("qwertyuiopasdfghjklzxcvbnmñ")


#ejercicio 15
def ordenar(cadena1):
    cadena1=cadena1.split("-")
    cadena1.sort()
    cadena2 = ""
    for w in cadena1:
        cadena2 += str(w) + "-"
    print(cadena2)

ordenar("green-red-yellow-black-white")

#EJERCICIO 16
def cuadrado(n):
    lista=[]
    while n>=1:
        lista.append(n**2)
        n-=1
    print(lista)
cuadrado(5)

#EJERCICIO 17
def ordenarnum(nums):
    nums.sort()
    print(nums[-1])

ordenarnum([1,2,3,4,67,89,54,21,1])

#EJERCICIO 18
def buscar(nums, numB):
    contador=0
    for n in nums:
        if numB==n:
            contador+=1
    print("El numero ", numB, "aparece ",contador,"veces")

buscar([1,2,3,4,3,2,3,4,5,6,56,7], 3)

#EJERCICIO 19 
def area (radio):
    import math
    a=math.pi*radio**2
    print("La area del ciculo :",a)

area(5)





#FIBONACCI
def fibonacci(l):
    l=l-2
    numeros=[0,1]
    while l>=1:
        n=numeros[-1]+numeros[-2]
        l-=1
        numeros.append(n)
    return numeros
print(fibonacci(5))





