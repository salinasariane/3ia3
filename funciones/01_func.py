#!/usr/bin/env python3


def hola_mundo(nombre):
    print('Hola mundo, mi nombre es', nombre)

hola_mundo('Ariane')

def display_message():
    print('Estamos en clase')
display_message()




def libro_favorito(titulo):
    print("Mi libro favorito es "+titulo)
libro_favorito("El rey leon") 



def make_shirt(talla, texto):
    print("La talla es ",talla, "y el texto es ",texto)

make_shirt("M", "Me llamo Ariane")
make_shirt(texto="Me llamo ariane", talla="M")




def make_shirt1(talla="xl", texto="Me encanta python"):
    print("La talla es ",talla, "y el texto es ",texto)

make_shirt1()
make_shirt1("L","Siifni")



def describe_city(ciudad, pais="Italia"):
    print(ciudad, "esta en ", pais)

describe_city("roma")
describe_city("lisboa", "Portugal")



def get_formatted_name(first_name, last_name):
    full_name = first_name + ' ' + last_name
    return full_name.title()

nombre=str(input("Introduce tu nombre "))
while nombre!="quit":
    apellido=str(input("Introduce tu Apellido "))
    while apellido!="quit":
        print(get_formatted_name(nombre, apellido))
        break
    break
    



def make_album(nombre, titulo, pistas=0):
    if pistas!=0:
       album={'nombre':nombre, 'titulo':titulo, 'pistas':pistas}        
    else:
       album={'nombre':nombre, 'tiutlo':titulo}
    return album
print (make_album("John","Verde"))
print (make_album("John","Verde",5))



magos=["Jon", "Mikel", "Laura"]
magos2=[]
def show_magicians(lista):
    for m in lista:
        print(m)

show_magicians(magos)

def make_great (lista, lista2):
    while len(lista):
        magos1=lista.pop()
        frase="El gran mago "+magos1
        lista2.append(frase)

make_great(magos, magos2)
show_magicians(magos2)


def sandwich(*ingredientes):
    for ing in ingredientes:
        print("- " + ing)

sandwich('pepperoni')
sandwich('jamon', 'queso', 'lechuga')
sandwich

def coche(marca, modelo, **info):
    car={}
    car['marca']=marca
    car['modelo']=modelo
    for key, value in info.items():
        car[key] = value
    return car

print(coche("vw", "golf", color='rojo'))
print(coche("vw", "golf"))