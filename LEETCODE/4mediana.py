class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        nums=nums1+nums2
        nums.sort()
        mediana =0.0
        if len(nums)%2-0.5==0.5:
            posicion_mediana=len(nums)/2-0.5
            mediana=nums[int(posicion_mediana)]   
        else:
            posicion_mediana=len(nums)/2
            num1=nums[int(posicion_mediana-0.5)]
            num2=nums[int(posicion_mediana+0.5)]
            mediana=(num1+num2)/2
        return mediana