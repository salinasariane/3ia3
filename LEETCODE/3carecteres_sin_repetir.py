class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        cadena=""
        for x in s:
            if x not in cadena:
                cadena+=x
        return len(cadena)