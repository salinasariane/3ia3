class Solution:
    def maxArea(self, height: List[int]) -> int:
        posible_resultado=[]
        for c in height:
            for y in height:
                x=height.index(y)-(height.index(c))       
                a=min(c,y)*x
                posible_resultado.append(a)
        posible_resultado.sort()
        return posible_resultado[-1]