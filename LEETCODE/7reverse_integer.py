class Solution:
    def reverse(self, x: int) -> int:
        reversed_int=int(str(x)[::-1])
        return reversed_int